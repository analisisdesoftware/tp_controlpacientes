package analisisdesoftware.tp.registropacientes.modelos.usuario;

import java.util.ArrayList;

import analisisdesoftware.tp.registropacientes.modelos.clasesauxiliares.Respuesta;
import analisisdesoftware.tp.registropacientes.modelos.informacionpersistente.InformacionPersistente;
import analisisdesoftware.tp.registropacientes.modelos.medico.Medico;
import analisisdesoftware.tp.registropacientes.modelos.paciente.Paciente;

/** Logica y acceso a datos del usuario */
public class UsuarioModelo {
	
	/** Obtener longitud maxima que puede tener un diagnostico
	 * @return Longitud maxima que puede tener un diagnostico */
	public static int getLongitudDiagnosticoMaxima(){
		return 50;
	}
	
	/** Valida si el usuario esta registrado y tiene permitido el acceso al sistema
	 * @param usuario Nombre del usuario
	 * @param password Password del usuario
	 * @return Indica si el usuario es o no valido */
	public static boolean loguear(String usuario, String password){
		return InformacionPersistente.loginUsuario(usuario, password);
	}
	
	public static Respuesta insertarSituacionPaciente(String codigo_paciente, String codigo_medico, String diagnostico){
		//Validar entrada
		if(!Paciente.validarLongitudCodigo(codigo_paciente))
			return new Respuesta(false, "El c�digo de paciente debe tener " + Paciente.CODIGO_LONGITUD + " caracteres");
		if(!Medico.validarLongitudCodigo(codigo_medico))
			return new Respuesta(false, "El c�digo de m�dico debe tener " + Paciente.CODIGO_LONGITUD + " caracteres");
		if(diagnostico.length() == 0 || diagnostico.length() > UsuarioModelo.getLongitudDiagnosticoMaxima())
			return new Respuesta(false, "El diagn�stico debe tener hasta " + UsuarioModelo.getLongitudDiagnosticoMaxima() + " caracteres");
		//Validar codigo existente
		if(!InformacionPersistente.existePaciente(codigo_paciente))
			return new Respuesta(false, "No existe un paciente con ese c�digo");
		if(!InformacionPersistente.existeMedico(codigo_medico))
			return new Respuesta(false, "No existe un m�dico con ese c�digo");
		//Insertar Paciente
		if(InformacionPersistente.insertarSituacionPaciente(codigo_paciente, codigo_medico, diagnostico)){
			return new Respuesta(true, "Situaci�n de paciente ingresada correctamente");									//Ingresado correctamente
		}else{
			return new Respuesta(false, "No pudo ingresarse la situaci�n del paciente. Cont�ctese con el administrador");	//No pudo ingresarse por excepcion
		}
	}

	public static ArrayList<String> obtenerPacientesAtendidos(String codigo_medico){
		//Validar entrada
		if(!Medico.validarLongitudCodigo(codigo_medico))
			return new ArrayList<String>();
		//Retorno
		return InformacionPersistente.obtenerPacientesAtendidos(codigo_medico);
	}
	public static ArrayList<String> obtenerEnfermedadesAtendidas(String codigo_medico){
		//Validar entrada
		if(!Medico.validarLongitudCodigo(codigo_medico))
			return new ArrayList<String>();
		//Retorno
		return InformacionPersistente.obtenerEnfermedadesAtendidas(codigo_medico);
	}
}
