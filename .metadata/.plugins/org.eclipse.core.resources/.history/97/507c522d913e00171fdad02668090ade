package analisisdesoftware.tp.registropacientes.modelos.usuario;

import java.util.ArrayList;

import analisisdesoftware.tp.controlpacientes.modelos.clasesauxiliares.Respuesta;
import analisisdesoftware.tp.controlpacientes.modelos.informacionpersistente.InformacionPersistente;
import analisisdesoftware.tp.controlpacientes.modelos.medico.Medico;
import analisisdesoftware.tp.registropacientes.modelos.paciente.Paciente;

/** Logica y acceso a datos del usuario */
public class UsuarioModelo {
	
	/** Valida si el usuario esta registrado y tiene permitido el acceso al sistema
	 * @param usuario Nombre del usuario
	 * @param password Password del usuario
	 * @return Indica si el usuario es o no valido */
	public static boolean loguear(String usuario, String password){
		return InformacionPersistente.loginUsuario(usuario, password);
	}
	
	/** Obtener longitud maxima que puede tener un diagnostico
	 * @return Longitud maxima que puede tener un diagnostico */
	public static int getLongitudDiagnosticoMaxima(){
		return 50;
	}
	
	/** Agrega una nueva situacion de paciente 
	 * @param codigo_paciente Codigo del paciente atendido
	 * @param codigo_medico Codigo del medico que atendio
	 * @param diagnostico Diagnostico del medico
	 * @return Indica si pudo o no realizar la insercion y asocia un mensaje donde se detalla el motivo del resultado */
	public static Respuesta insertarSituacionPaciente(String codigo_paciente, String codigo_medico, String diagnostico){
		//Validar entrada
		if(!Paciente.validarLongitudCodigo(codigo_paciente))
			return new Respuesta(false, "El c�digo de paciente debe tener " + Paciente.CODIGO_LONGITUD + " caracteres");
		if(!Medico.validarLongitudCodigo(codigo_medico))
			return new Respuesta(false, "El c�digo de m�dico debe tener " + Paciente.CODIGO_LONGITUD + " caracteres");
		if(diagnostico.length() == 0 || diagnostico.length() > UsuarioModelo.getLongitudDiagnosticoMaxima())
			return new Respuesta(false, "El diagn�stico debe tener hasta " + UsuarioModelo.getLongitudDiagnosticoMaxima() + " caracteres");
		//Validar codigo existente
		if(!InformacionPersistente.existePaciente(codigo_paciente))
			return new Respuesta(false, "No existe un paciente con ese c�digo");
		if(!InformacionPersistente.existeMedico(codigo_medico))
			return new Respuesta(false, "No existe un m�dico con ese c�digo");
		//Insertar Paciente
		if(InformacionPersistente.insertarSituacionPaciente(codigo_paciente, codigo_medico, diagnostico)){
			return new Respuesta(true, "Situaci�n de paciente ingresada correctamente");									//Ingresado correctamente
		}else{
			return new Respuesta(false, "No pudo ingresarse la situaci�n del paciente. Cont�ctese con el administrador");	//No pudo ingresarse por excepcion
		}
	}

	/** Obtiene la lista de pacientes atendidos por un medico
	 * @param codigo_medico Codigo del medico
	 * @return Lista con el nombre de los pacientes atendidos por el medico */
	public static ArrayList<String> obtenerPacientesAtendidos(String codigo_medico){
		//Validar entrada
		if(!Medico.validarLongitudCodigo(codigo_medico))
			return new ArrayList<String>();
		//Retorno
		return InformacionPersistente.obtenerPacientesAtendidos(codigo_medico);
	}
	/** Obtiene la lista de enfermedades que atendio un medico
	 * @param codigo_medico Codigo del medico
	 * @return Lista de diagnosticos que el medico determino*/
	public static ArrayList<String> obtenerEnfermedadesAtendidas(String codigo_medico){
		//Validar entrada
		if(!Medico.validarLongitudCodigo(codigo_medico))
			return new ArrayList<String>();
		//Retorno
		return InformacionPersistente.obtenerEnfermedadesAtendidas(codigo_medico);
	}
}
