package analisisdesoftware.tp.registropacientes.modelos.informacionpersistente;
import java.io.File;
import java.sql.*;
import java.util.ArrayList;

import analisisdesoftware.tp.registropacientes.modelos.paciente.Paciente;

public class InformacionPersistente {	
	public final static String DB_FILENAME= "controlpacientes.db";
	private final static String ADMIN_DEFAULT_PASSWORD = "admin";
	
	/** Crea la base de datos en caso de no existir
	 * @return Indica si pudo o no crear la base de datos */
	public static boolean inicializarBaseDatos(){
		File f = new File(DB_FILENAME);
		if(!f.exists() || f.isDirectory()) { 	
			Connection c = null;
			Statement stmt = null;
			String sql;
			try {
				//Crear e iniciar conexion con base de datos
				Class.forName("org.sqlite.JDBC");
				c = DriverManager.getConnection("jdbc:sqlite:" + DB_FILENAME);
				stmt = c.createStatement();
				//Crear Tabla Usuario
			    sql = "CREATE TABLE usuario" +
	                  "(id 			INT		PRIMARY KEY	NOT NULL," +
			          " nombre     	TEXT    NOT NULL CHECK(length(nombre) <= 50), " + 
			          " password   	TEXT    NOT NULL CHECK(length(password) <= 50))";
			    stmt.executeUpdate(sql);
			    //Insertar Usuario ADMIN default
			    sql = "INSERT INTO usuario (id,nombre,password) " + 
			    	  "VALUES(1,'admin','" + ADMIN_DEFAULT_PASSWORD + "')";
			    stmt.executeUpdate(sql);
			    //Crear Tabla Paciente
			    sql = "CREATE TABLE paciente" +
		                  "(codigo 		TEXT	PRIMARY KEY	NOT NULL CHECK(length(codigo) == 5)," +
				          " nombre   	TEXT    NOT NULL CHECK(length(nombre) <= 50))";
			    stmt.executeUpdate(sql);
			    //Crear Tabla Medico
			    sql = "CREATE TABLE medico" +
		                  "(codigo  			INT		PRIMARY KEY	NOT NULL CHECK(length(codigo) == 5)," +
				          " nombre   			TEXT    NOT NULL CHECK(length(nombre) <= 50)," +
				          " especializacion   	TEXT    NOT NULL)";
			    stmt.executeUpdate(sql);
			  //Crear Tabla SituacionPaciente
			    sql = "CREATE TABLE situacionpaciente" +
		                  "(id 		 		INT		PRIMARY KEY	NOT NULL," +
				          " codigo_paciente	TEXT	NOT NULL," + 
				          " codigo_medico	TEXT	NOT NULL," +
				          " diagnostico		TEXT	NOT NULL," +
				          " FOREIGN KEY(codigo_paciente) REFERENCES paciente(codigo)," +
				          " FOREIGN KEY(codigo_medico) REFERENCES medico(codigo) )";
			    stmt.executeUpdate(sql);
			    
				return true;
		    } catch ( Exception e ) {
		    	System.err.println("Excepcion: " + e.getClass().getName() + ": " + e.getMessage() );
		    	return false;
		    } finally{
		    	//Liberar memoria
		    	if(stmt != null){
			    	try {
						stmt.close();
					} catch (SQLException e) {}
		    	}
		    	if(c != null){
				    try {
						c.close();
					} catch (SQLException e) {}
		    	}
		    }
		}
		return true;
	}
	public static boolean loginUsuario(String usuario, String password){
		Connection c = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		String sql;
		try {
			//Crear e iniciar conexion con base de datos
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + DB_FILENAME);
			c.setAutoCommit(false);
			//Consultar
			sql = "SELECT 1 FROM usuario WHERE nombre = ? AND password = ? LIMIT 1";	//Consultar si hay un usuario con ese nombre y contrasena
			stmt = c.prepareStatement(sql);
			stmt.setString(1, usuario);
			stmt.setString(2, password);
			rs = stmt.executeQuery();
			if(rs.next())
				return true;
			return false;
	    } catch ( Exception e ) {
	    	System.err.println("Excepcion: " + e.getClass().getName() + ": " + e.getMessage() );
	    	return false;
	    } finally{
	    	//Liberar memoria
	    	if(stmt != null){
		    	try {
					stmt.close();
				} catch (SQLException e) {}
	    	}
	    	if(c != null){
			    try {
					c.close();
				} catch (SQLException e) {}
	    	}
	    	if(rs != null){
	    		try {
					rs.close();
				} catch (SQLException e) {}
	    	}
	    }
	}
	
	public static boolean existePaciente(String codigopaciente){
		Connection c = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		String sql;
		try {
			//Crear e iniciar conexion con base de datos
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + DB_FILENAME);
			c.setAutoCommit(false);
			//Consultar
			sql = "SELECT 1 FROM paciente WHERE codigo = ? LIMIT 1";	//Consultar si existe paciente con ese codigo
			stmt = c.prepareStatement(sql);
			stmt.setString(1, codigopaciente);
			rs = stmt.executeQuery();
			if(rs.next())
				return false;
			return true;
	    } catch ( Exception e ) {
	    	System.err.println("Excepcion: " + e.getClass().getName() + ": " + e.getMessage() );
	    	return false;
	    } finally{
	    	//Liberar memoria
	    	if(stmt != null){
		    	try {
					stmt.close();
				} catch (SQLException e) {}
	    	}
	    	if(c != null){
			    try {
					c.close();
				} catch (SQLException e) {}
	    	}
	    	if(rs != null){
	    		try {
					rs.close();
				} catch (SQLException e) {}
	    	}
	    }
	}
	public static boolean insertarPaciente(String codigo, String nombre){
		Connection c = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		String sql;
		try {
			//Crear e iniciar conexion con base de datos
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + DB_FILENAME);
			//Consultar
			sql = "INSERT INTO paciente (codigo,nombre) " + 
		    	  "VALUES(?,?)";
			stmt = c.prepareStatement(sql);
			stmt.setString(1, codigo);
			stmt.setString(2, nombre);
			stmt.executeUpdate();
			return true;
	    } catch ( Exception e ) {
	    	System.err.println("Excepcion: " + e.getClass().getName() + ": " + e.getMessage() );
	    	return false;
	    } finally{
	    	//Liberar memoria
	    	if(stmt != null){
		    	try {
					stmt.close();
				} catch (SQLException e) {}
	    	}
	    	if(c != null){
			    try {
					c.close();
				} catch (SQLException e) {}
	    	}
	    	if(rs != null){
	    		try {
					rs.close();
				} catch (SQLException e) {}
	    	}
	    }
	}
	public static ArrayList<Paciente> obtenerPacientes(){
		ArrayList<Paciente> pacientes = new ArrayList<>();
		Connection c = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		String sql;
		try {
			//Crear e iniciar conexion con base de datos
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + DB_FILENAME);
			c.setAutoCommit(false);
			//Consultar
			sql = "SELECT codigo,nombre FROM paciente";	//Obtener todos los pacientes
			stmt = c.prepareStatement(sql);
			rs = stmt.executeQuery();
			while(rs.next()){
				Paciente paciente = new Paciente(rs.getString(0), rs.getString(1));
				pacientes.add(paciente);
			}
	    } catch ( Exception e ) {
	    	System.err.println("Excepcion: " + e.getClass().getName() + ": " + e.getMessage() );
	    } finally{
	    	//Liberar memoria
	    	if(stmt != null){
		    	try {
					stmt.close();
				} catch (SQLException e) {}
	    	}
	    	if(c != null){
			    try {
					c.close();
				} catch (SQLException e) {}
	    	}
	    	if(rs != null){
	    		try {
					rs.close();
				} catch (SQLException e) {}
	    	}
	    }
		return pacientes;
	}
}
