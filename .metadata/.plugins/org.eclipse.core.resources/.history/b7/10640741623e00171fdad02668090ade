package analisisdesoftware.tp.registropacientes.vistas.ingresardatos.situacionpaciente;

import javax.swing.JDialog;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JTextField;

import analisisdesoftware.tp.registropacientes.controladores.MedicoControlador;
import analisisdesoftware.tp.registropacientes.controladores.PacienteControlador;
import analisisdesoftware.tp.registropacientes.controladores.UsuarioControlador;
import analisisdesoftware.tp.registropacientes.modelos.clasesauxiliares.Respuesta;
import analisisdesoftware.tp.registropacientes.modelos.medico.Medico;
import analisisdesoftware.tp.registropacientes.modelos.paciente.Paciente;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.FormSpecs;

public class FormIngresarSituacion extends JDialog{
	
	private static final long serialVersionUID = 1956306470327524280L;
	private JComboBox<Paciente> cbxPaciente;
	private JComboBox<Medico> cbxMedico;
	private JTextField txtDiagnostico;
	private JButton btnIngresar;
	
	public FormIngresarSituacion() {
		ArrayList<Paciente> pacientes;
		ArrayList<Medico> medicos;
		setModalityType(ModalityType.APPLICATION_MODAL);
		setTitle("Centro M\u00E9dico Los Laureles - Situaci\u00F3n Paciente");
		initialize();
		
		//Cargar Interfaz
		pacientes = PacienteControlador.obtenerPacientes();
		medicos = MedicoControlador.obtenerMedicos();
		cargarPacientes(pacientes);
		cargarMedicos(medicos);
		if(pacientes.size() == 0 || medicos.size() == 0){
			txtDiagnostico.setEnabled(false);
			btnIngresar.setEnabled(false);
		}
	}

	private void initialize() {
		setBounds(100, 100, 431, 157);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		
		btnIngresar = new JButton("Ingresar");
		btnIngresar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ingresarSituacionPaciente();
			}
		});
		getContentPane().add(btnIngresar, BorderLayout.SOUTH);
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(new FormLayout(new ColumnSpec[] {
				FormSpecs.RELATED_GAP_COLSPEC,
				FormSpecs.DEFAULT_COLSPEC,
				FormSpecs.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),},
			new RowSpec[] {
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,}));
		
		JLabel lblPaciente = new JLabel("Paciente");
		panel.add(lblPaciente, "2, 2, right, default");
		
		cbxPaciente = new JComboBox<Paciente>();
		panel.add(cbxPaciente, "4, 2, fill, default");
		
		JLabel lblMedico = new JLabel("M\u00E9dico");
		panel.add(lblMedico, "2, 4, right, default");
		
		cbxMedico = new JComboBox<Medico>();
		panel.add(cbxMedico, "4, 4, fill, default");
		
		JLabel lblDiagnstico = new JLabel("Diagn\u00F3stico");
		panel.add(lblDiagnstico, "2, 6, right, default");
		
		txtDiagnostico = new JTextField();
		panel.add(txtDiagnostico, "4, 6, fill, default");
		txtDiagnostico.setColumns(10);
	}
	
	private void cargarPacientes(ArrayList<Paciente> pacientes){
		for (Paciente paciente : pacientes) {
			cbxPaciente.addItem(paciente);
		}
	}
	private void cargarMedicos(ArrayList<Medico> medicos){
		for (Medico medico : medicos) {
			cbxMedico.addItem(medico);
		}
	}
	private void ingresarSituacionPaciente(){
		Paciente paciente = (Paciente) cbxPaciente.getSelectedItem();
		Medico medico = (Medico) cbxMedico.getSelectedItem();
		String diagnostico = txtDiagnostico.getText().trim();
		//Validaciones
		if(diagnostico.length() == 0){
			JOptionPane.showMessageDialog(null, "Debe ingresar el diagnůstico");
			txtDiagnostico.requestFocus();
			return;
		}
		//Insertar
		Respuesta respuesta = UsuarioControlador.insertarSituacionPaciente(paciente.getCodigo(), medico.getCodigo(), diagnostico);
		JOptionPane.showMessageDialog(null, respuesta.getMensaje());
		if(respuesta.fueSatisfactoria()){
			txtDiagnostico.setText("");
			cbxPaciente.requestFocus();
		}
	}
}
