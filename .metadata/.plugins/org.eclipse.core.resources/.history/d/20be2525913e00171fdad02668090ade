package analisisdesoftware.tp.registropacientes.modelos.informacionpersistente;
import java.io.File;
import java.sql.*;
import java.util.ArrayList;

import analisisdesoftware.tp.registropacientes.modelos.medico.Medico;
import analisisdesoftware.tp.registropacientes.modelos.paciente.Paciente;

/** Almacen de datos implementado con SQLite */
public class InformacionPersistente {
	/** Nombre del archivo de base de datos donde se almacenara la informacion */
	public final static String DB_FILENAME= "controlpacientes.db";
	/** Contraseņa por defecto para el usuario "admin" al momento de crease la base de datos */
	private final static String ADMIN_DEFAULT_PASSWORD = "admin";
	
	/** Cierra y libera los recursos de la conexion 
	 * @param c Conexion a cerrar*/
	private static void cerrarConexion(Connection c){
		if(c != null){
		    try {
				c.close();
			} catch (SQLException e) {}
    	}
	}
	/** Cierra y libera los recursos de la conexion 
	 * @param c Conexion a cerrar
	 * @param stmt Consulta a liberar*/
	private static void cerrarConexion(Connection c, PreparedStatement stmt){
		if(stmt != null){
	    	try {
				stmt.close();
			} catch (SQLException e) {}
    	}
		cerrarConexion(c);
	}
	/** Cierra y libera los recursos de la conexion 
	 * @param c Conexion a cerrar
	 * @param stmt Consulta a liberar
	 * @param rs RecordSet a liberar*/
	private static void cerrarConexion(Connection c, PreparedStatement stmt, ResultSet rs){
		//Liberar memoria
    	if(rs != null){
    		try {
				rs.close();
			} catch (SQLException e) {}
    	}
    	cerrarConexion(c,stmt);
	}
	
	/** Crea la base de datos en caso de no existir
	 * @return Indica si pudo o no crear la base de datos */
	public static boolean inicializarBaseDatos(){
		File f = new File(DB_FILENAME);
		if(!f.exists() || f.isDirectory()) { 	
			Connection c = null;
			Statement stmt = null;
			String sql;
			try {
				//Crear e iniciar conexion con base de datos
				Class.forName("org.sqlite.JDBC");
				c = DriverManager.getConnection("jdbc:sqlite:" + DB_FILENAME);
				stmt = c.createStatement();
				//Crear Tabla Usuario
			    sql = "CREATE TABLE usuario" +
	                  "(id 			INTEGER	PRIMARY KEY	NOT NULL," +
			          " nombre     	TEXT   	NOT NULL CHECK(length(nombre) <= 50), " + 
			          " password   	TEXT    NOT NULL CHECK(length(password) <= 50))";
			    stmt.executeUpdate(sql);
			    //Insertar Usuario ADMIN default
			    sql = "INSERT INTO usuario (id,nombre,password) " + 
			    	  "VALUES(1,'admin','" + ADMIN_DEFAULT_PASSWORD + "')";
			    stmt.executeUpdate(sql);
			    //Crear Tabla Paciente
			    sql = "CREATE TABLE paciente" +
		                  "(codigo 		TEXT	PRIMARY KEY	NOT NULL CHECK(length(codigo) == 5)," +
				          " nombre   	TEXT    NOT NULL CHECK(length(nombre) <= 50))";
			    stmt.executeUpdate(sql);
			    //Crear Tabla Medico
			    sql = "CREATE TABLE medico" +
		                  "(codigo  			TEXT	PRIMARY KEY	NOT NULL CHECK(length(codigo) == 5)," +
				          " nombre   			TEXT    NOT NULL CHECK(length(nombre) <= 50)," +
				          " especializacion   	TEXT    NOT NULL CHECK(length(especializacion) <= 50))";
			    stmt.executeUpdate(sql);
			  //Crear Tabla SituacionPaciente
			    sql = "CREATE TABLE situacionpaciente" +
		                  "(id 		 		INTEGER		PRIMARY KEY	AUTOINCREMENT	NOT NULL," +
				          " codigo_paciente	TEXT	NOT NULL," + 
				          " codigo_medico	TEXT	NOT NULL," +
				          " diagnostico		TEXT	NOT NULL," +
				          " FOREIGN KEY(codigo_paciente) REFERENCES paciente(codigo)," +
				          " FOREIGN KEY(codigo_medico) REFERENCES medico(codigo) );" +
				          " CREATE INDEX idxcodigo_paciente ON situacionpaciente(codigo_medico);";
			    stmt.executeUpdate(sql);
			    
				return true;
		    } catch ( Exception e ) {
		    	System.err.println("Excepcion: " + e.getClass().getName() + ": " + e.getMessage() );
		    	return false;
		    } finally{
		    	//Liberar memoria
		    	if(stmt != null){
			    	try {
						stmt.close();
					} catch (SQLException e) {}
		    	}
		    	if(c != null){
				    try {
						c.close();
					} catch (SQLException e) {}
		    	}
		    }
		}
		return true;
	}
	
	/** Valida si el usuario esta registrado y tiene permitido el acceso al sistema
	 * @param usuario Nombre del usuario
	 * @param password Password del usuario
	 * @return Indica si el usuario es o no valido */
	public static boolean loginUsuario(String usuario, String password){
		Connection c = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		String sql;
		try {
			//Crear e iniciar conexion con base de datos
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + DB_FILENAME);
			c.setAutoCommit(false);
			//Consultar
			sql = "SELECT 1 FROM usuario WHERE nombre = ? AND password = ? LIMIT 1";	//Consultar si hay un usuario con ese nombre y contrasena
			stmt = c.prepareStatement(sql);
			stmt.setString(1, usuario);
			stmt.setString(2, password);
			rs = stmt.executeQuery();
			if(rs.next())
				return true;
			return false;
	    } catch ( Exception e ) {
	    	System.err.println("Excepcion: " + e.getClass().getName() + ": " + e.getMessage() );
	    	return false;
	    } finally{
	    	//Liberar memoria
	    	cerrarConexion(c,stmt,rs);
	    }
	}
	
	/** Valida si un paciente existe
	 * @param codigopaciente Codigo del paciente
	 * @return True: El paciente existe. False: Paciente inexistente */
	public static boolean existePaciente(String codigopaciente){
		Connection c = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		String sql;
		try {
			//Crear e iniciar conexion con base de datos
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + DB_FILENAME);
			c.setAutoCommit(false);
			//Consultar
			sql = "SELECT 1 FROM paciente WHERE codigo = ? LIMIT 1";	//Consultar si existe paciente con ese codigo
			stmt = c.prepareStatement(sql);
			stmt.setString(1, codigopaciente);
			rs = stmt.executeQuery();
			if(rs.next())
				return true;
			return false;
	    } catch ( Exception e ) {
	    	System.err.println("Excepcion: " + e.getClass().getName() + ": " + e.getMessage() );
	    	return false;
	    } finally{
	    	//Liberar memoria
	    	cerrarConexion(c,stmt,rs);
	    }
	}
	/** Agrega un nuevo paciente
	 * @param codigo Codigo del paciente
	 * @param nombre Nombre del paciente
	 * @return Indica si pudo o no realizar la insercion y asocia un mensaje donde se detalla el motivo del resultado */
	public static boolean insertarPaciente(String codigo, String nombre){
		Connection c = null;
		PreparedStatement stmt = null;
		String sql;
		try {
			//Crear e iniciar conexion con base de datos
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + DB_FILENAME);
			//Consultar
			sql = "INSERT INTO paciente (codigo,nombre) " + 
		    	  "VALUES(?,?)";
			stmt = c.prepareStatement(sql);
			stmt.setString(1, codigo);
			stmt.setString(2, nombre);
			stmt.executeUpdate();
			return true;
	    } catch ( Exception e ) {
	    	System.err.println("Excepcion: " + e.getClass().getName() + ": " + e.getMessage() );
	    	return false;
	    } finally{
	    	//Liberar memoria
	    	cerrarConexion(c,stmt);
	    }
	}
	/** Obtiene el listado completo de pacientes
	 * @return Lista de pacientes */
	public static ArrayList<Paciente> obtenerPacientes(){
		ArrayList<Paciente> pacientes = new ArrayList<>();
		Connection c = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		String sql;
		try {
			//Crear e iniciar conexion con base de datos
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + DB_FILENAME);
			c.setAutoCommit(false);
			//Consultar
			sql = "SELECT codigo,nombre FROM paciente";	//Obtener todos los pacientes
			stmt = c.prepareStatement(sql);
			rs = stmt.executeQuery();
			while(rs.next()){
				Paciente paciente = new Paciente(rs.getString(1), rs.getString(2));
				pacientes.add(paciente);
			}
	    } catch ( Exception e ) {
	    	System.err.println("Excepcion: " + e.getClass().getName() + ": " + e.getMessage() );
	    } finally{
	    	//Liberar memoria
	    	cerrarConexion(c,stmt,rs);
	    }
		return pacientes;
	}

	/** Valida si un medico existe
	 * @param codigomedico Codigo del medico
	 * @return True: El medico existe. False: Medico inexistente */
	public static boolean existeMedico(String codigomedico){
		Connection c = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		String sql;
		try {
			//Crear e iniciar conexion con base de datos
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + DB_FILENAME);
			c.setAutoCommit(false);
			//Consultar
			sql = "SELECT 1 FROM medico WHERE codigo = ? LIMIT 1";	//Consultar si existe paciente con ese codigo
			stmt = c.prepareStatement(sql);
			stmt.setString(1, codigomedico);
			rs = stmt.executeQuery();
			if(rs.next())
				return true;
			return false;
	    } catch ( Exception e ) {
	    	System.err.println("Excepcion: " + e.getClass().getName() + ": " + e.getMessage() );
	    	return false;
	    } finally{
	    	//Liberar memoria
	    	cerrarConexion(c,stmt,rs);
	    }
	}
	
	/** Agrega un nuevo medico
	 * @param codigo Codigo del medico
	 * @param nombre Nombre del medico
	 * @param especializacion Especializacion del medico
	 * @return Indica si pudo o no realizar la insercion y asocia un mensaje donde se detalla el motivo del resultado */
	public static boolean insertarMedico(String codigo, String nombre, String especializacion){
		Connection c = null;
		PreparedStatement stmt = null;
		String sql;
		try {
			//Crear e iniciar conexion con base de datos
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + DB_FILENAME);
			//Consultar
			sql = "INSERT INTO medico (codigo,nombre,especializacion) " + 
		    	  "VALUES(?,?,?)";
			stmt = c.prepareStatement(sql);
			stmt.setString(1, codigo);
			stmt.setString(2, nombre);
			stmt.setString(3, especializacion);
			stmt.executeUpdate();
			return true;
	    } catch ( Exception e ) {
	    	System.err.println("Excepcion: " + e.getClass().getName() + ": " + e.getMessage() );
	    	return false;
	    } finally{
	    	//Liberar memoria
	    	cerrarConexion(c,stmt);
	    }
	}
	/** Obtiene el listado completo de medicos
	 * @return Lista de medicos */
	public static ArrayList<Medico> obtenerMedicos(){
		ArrayList<Medico> medicos = new ArrayList<>();
		Connection c = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		String sql;
		try {
			//Crear e iniciar conexion con base de datos
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + DB_FILENAME);
			c.setAutoCommit(false);
			//Consultar
			sql = "SELECT codigo,nombre,especializacion FROM medico";	//Obtener todos los pacientes
			stmt = c.prepareStatement(sql);
			rs = stmt.executeQuery();
			while(rs.next()){
				Medico medico = new Medico(rs.getString(1), rs.getString(2), rs.getString(3));
				medicos.add(medico);
			}
	    } catch ( Exception e ) {
	    	System.err.println("Excepcion: " + e.getClass().getName() + ": " + e.getMessage() );
	    } finally{
	    	//Liberar memoria
	    	cerrarConexion(c,stmt,rs);
	    }
		return medicos;
	}

	/** Agrega una nueva situacion de paciente 
	 * @param codigo_paciente Codigo del paciente atendido
	 * @param codigo_medico Codigo del medico que atendio
	 * @param diagnostico Diagnostico del medico
	 * @return Indica si pudo o no realizar la insercion y asocia un mensaje donde se detalla el motivo del resultado */
	public static boolean insertarSituacionPaciente(String codigopaciente, String codigomedico, String diagnostico){
		Connection c = null;
		PreparedStatement stmt = null;
		String sql;
		try {
			//Crear e iniciar conexion con base de datos
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + DB_FILENAME);
			//Consultar
			sql = "INSERT INTO situacionpaciente (codigo_paciente,codigo_medico,diagnostico) " + 
		    	  "VALUES(?,?,?)";
			stmt = c.prepareStatement(sql);
			stmt.setString(1, codigopaciente);
			stmt.setString(2, codigomedico);
			stmt.setString(3, diagnostico);
			stmt.executeUpdate();
			return true;
	    } catch ( Exception e ) {
	    	System.err.println("Excepcion: " + e.getClass().getName() + ": " + e.getMessage() );
	    	return false;
	    } finally{
	    	//Liberar memoria
	    	cerrarConexion(c,stmt);
	    }
	}
	/** Obtiene la lista de pacientes atendidos por un medico
	 * @param codigo_medico Codigo del medico
	 * @return Lista con el nombre de los pacientes atendidos por el medico */
	public static ArrayList<String> obtenerPacientesAtendidos(String codigo_medico){
		ArrayList<String> nombrepacientes = new ArrayList<>();
		Connection c = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		String sql;
		try {
			//Crear e iniciar conexion con base de datos
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + DB_FILENAME);
			c.setAutoCommit(false);
			//Consultar
			sql = "SELECT DISTINCT p.nombre "
				+ "FROM situacionpaciente AS sp "
				+ "LEFT JOIN paciente AS p ON p.codigo = sp.codigo_paciente "
				+ "WHERE sp.codigo_medico = ? "
				+ "ORDER BY p.nombre";
			stmt = c.prepareStatement(sql);
			stmt.setString(1, codigo_medico);
			rs = stmt.executeQuery();
			while(rs.next()){
				nombrepacientes.add(rs.getString(1));
			}
	    } catch ( Exception e ) {
	    	System.err.println("Excepcion: " + e.getClass().getName() + ": " + e.getMessage() );
	    } finally{
	    	//Liberar memoria
	    	cerrarConexion(c,stmt,rs);
	    }
		return nombrepacientes;
	}
	/** Obtiene la lista de enfermedades que atendio un medico
	 * @param codigo_medico Codigo del medico
	 * @return Lista de diagnosticos que el medico determino*/
	public static ArrayList<String> obtenerEnfermedadesAtendidas(String codigo_medico){
		ArrayList<String> enfermedades = new ArrayList<>();
		Connection c = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		String sql;
		try {
			//Crear e iniciar conexion con base de datos
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + DB_FILENAME);
			c.setAutoCommit(false);
			//Consultar
			sql = "SELECT DISTINCT diagnostico "
				+ "FROM situacionpaciente "
				+ "WHERE codigo_medico = ? "
				+ "ORDER BY diagnostico";
			stmt = c.prepareStatement(sql);
			stmt.setString(1, codigo_medico);
			rs = stmt.executeQuery();
			while(rs.next()){
				enfermedades.add(rs.getString(1));
			}
	    } catch ( Exception e ) {
	    	System.err.println("Excepcion: " + e.getClass().getName() + ": " + e.getMessage() );
	    } finally{
	    	//Liberar memoria
	    	cerrarConexion(c,stmt,rs);
	    }
		return enfermedades;
	}

}
