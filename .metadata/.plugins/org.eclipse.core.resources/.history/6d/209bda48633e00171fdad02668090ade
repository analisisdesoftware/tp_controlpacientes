package analisisdesoftware.tp.registropacientes.vistas.ingresardatos.situacionpaciente;

import javax.swing.JDialog;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JTextField;

import analisisdesoftware.tp.registropacientes.controladores.MedicoControlador;
import analisisdesoftware.tp.registropacientes.controladores.PacienteControlador;
import analisisdesoftware.tp.registropacientes.controladores.UsuarioControlador;
import analisisdesoftware.tp.registropacientes.modelos.clasesauxiliares.Respuesta;
import analisisdesoftware.tp.registropacientes.modelos.medico.Medico;
import analisisdesoftware.tp.registropacientes.modelos.paciente.Paciente;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.FormSpecs;

public class FormIngresarSituacion extends JDialog{
	
	private static final long serialVersionUID = 1956306470327524280L;
	private JComboBox<Paciente> cbxPaciente;
	private JComboBox<Medico> cbxMedico;
	private JTextField txtDiagnostico;
	private JButton btnIngresar;
	
	public FormIngresarSituacion() {
		setResizable(false);
		ArrayList<Paciente> pacientes;
		ArrayList<Medico> medicos;
		setModalityType(ModalityType.APPLICATION_MODAL);
		setTitle("Centro M\u00E9dico Los Laureles - Situaci\u00F3n Paciente");
		initialize();
		
		//Cargar Interfaz
		pacientes = PacienteControlador.obtenerPacientes();
		medicos = MedicoControlador.obtenerMedicos();
		cargarPacientes(pacientes);
		cargarMedicos(medicos);
		if(pacientes.size() == 0 || medicos.size() == 0){
			txtDiagnostico.setEnabled(false);
			btnIngresar.setEnabled(false);
		}
	}

	private void initialize() {
		setBounds(100, 100, 431, 157);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		
		btnIngresar = new JButton("Ingresar");
		btnIngresar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ingresarSituacionPaciente();
			}
		});
		getContentPane().add(btnIngresar, BorderLayout.SOUTH);
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		JLabel lblPaciente = new JLabel("Paciente");
		lblPaciente.setBounds(20, 9, 41, 14);
		panel.add(lblPaciente);
		
		cbxPaciente = new JComboBox<Paciente>();
		cbxPaciente.setBounds(67, 6, 358, 20);
		panel.add(cbxPaciente);
		
		JLabel lblMedico = new JLabel("M\u00E9dico");
		lblMedico.setBounds(28, 35, 33, 14);
		panel.add(lblMedico);
		
		cbxMedico = new JComboBox<Medico>();
		cbxMedico.setBounds(67, 32, 358, 20);
		panel.add(cbxMedico);
		
		JLabel lblDiagnstico = new JLabel("Diagn\u00F3stico");
		lblDiagnstico.setBounds(6, 61, 55, 14);
		panel.add(lblDiagnstico);
		
		txtDiagnostico = new JTextField();
		txtDiagnostico.setBounds(67, 58, 358, 20);
		panel.add(txtDiagnostico);
		txtDiagnostico.setColumns(10);
	}
	
	private void cargarPacientes(ArrayList<Paciente> pacientes){
		for (Paciente paciente : pacientes) {
			cbxPaciente.addItem(paciente);
		}
	}
	private void cargarMedicos(ArrayList<Medico> medicos){
		for (Medico medico : medicos) {
			cbxMedico.addItem(medico);
		}
	}
	private void ingresarSituacionPaciente(){
		Paciente paciente = (Paciente) cbxPaciente.getSelectedItem();
		Medico medico = (Medico) cbxMedico.getSelectedItem();
		String diagnostico = txtDiagnostico.getText().trim();
		//Validaciones
		if(diagnostico.length() == 0){
			JOptionPane.showMessageDialog(null, "Debe ingresar el diagnůstico");
			txtDiagnostico.requestFocus();
			return;
		}
		//Insertar
		Respuesta respuesta = UsuarioControlador.insertarSituacionPaciente(paciente.getCodigo(), medico.getCodigo(), diagnostico);
		JOptionPane.showMessageDialog(null, respuesta.getMensaje());
		if(respuesta.fueSatisfactoria()){
			txtDiagnostico.setText("");
			cbxPaciente.requestFocus();
		}
	}
}
