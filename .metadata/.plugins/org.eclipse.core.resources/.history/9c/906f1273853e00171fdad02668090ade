package analisisdesoftware.tp.registropacientes.vistas.ingresardatos.datosmedico;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Dialog.ModalityType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import analisisdesoftware.tp.registropacientes.controladores.MedicoControlador;
import analisisdesoftware.tp.registropacientes.modelos.clasesauxiliares.Respuesta;
import analisisdesoftware.tp.registropacientes.modelos.medico.Medico;

public class FormIngresoMedico extends JDialog{

	private JTable table;
	private DefaultTableModel modeltable;
	private JTextField txtCodigo;
	private JTextField txtNombre;
	private JTextField txtEspecializacion;
	
	private static int codigoLongitud;
	private static int nombreMaximaLongitud;
	private static int especializacionMaximaLongitud;
	
	
	public FormIngresoMedico() {
		setModalityType(ModalityType.APPLICATION_MODAL);
		//Cargar longitudes
		codigoLongitud = MedicoControlador.getLongitudCodigo();
		nombreMaximaLongitud = MedicoControlador.getLongitudNombreMaxima();
		especializacionMaximaLongitud = MedicoControlador.getLongitudEspecializacionMaxima();
		
		setTitle("Centro M\u00E9dico Los Laureles - Datos de M�dicos");
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.NORTH);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel lblCodigo = new JLabel("C\u00F3digo");
		panel.add(lblCodigo);
		
		txtCodigo = new JTextField();
		panel.add(txtCodigo);
		txtCodigo.setColumns(10);
		txtCodigo.addKeyListener(new KeyAdapter() {
		    public void keyTyped(KeyEvent e) { 
		        if (txtCodigo.getText().length() == codigoLongitud ) //Limitar caracteres
		            e.consume(); 
		    }  
		});
		
		JLabel lblNombre = new JLabel("Nombre");
		panel.add(lblNombre);
		
		txtNombre = new JTextField();
		txtNombre.setText("");
		panel.add(txtNombre);
		txtNombre.setColumns(10);
		txtNombre.addKeyListener(new KeyAdapter() {			
		    public void keyTyped(KeyEvent e) { 
		    	
		        if (txtNombre.getText().length() >= nombreMaximaLongitud ) //Limitar caracteres
		            e.consume(); 
		    }  
		});
		
		JButton btnIngresar = new JButton("Ingresar");
		btnIngresar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String codigo = txtCodigo.getText().trim();
				String nombre = txtNombre.getText().trim();
				String especialidad = txtEspecializacion.getText().trim();
				//Validar que haya caracteres
				if(codigo.length() != codigoLongitud){
					JOptionPane.showMessageDialog(null, "Ingrese el c�digo de medico de " + codigoLongitud + " caracteres");
					return;
				}
				if(nombre.length() == 0 || nombre.length() > nombreMaximaLongitud){
					JOptionPane.showMessageDialog(null, "El nombre del medico debe tener como mucho " + nombreMaximaLongitud + " caracteres");
					return;
				}
				if(especialidad.length() == 0 || especialidad.length() > especializacionMaximaLongitud){
					JOptionPane.showMessageDialog(null, "La especializacion del medico debe tener como mucho " + especializacionMaximaLongitud + " caracteres");
					return;
				}
				//Insertar
				Respuesta respuesta = MedicoControlador.insertarMedico(codigo, nombre, especialidad);
				JOptionPane.showMessageDialog(null, respuesta.getMensaje());
				if(respuesta.fueSatisfactoria()){
					agregarMedico(codigo, nombre, especialidad);
					txtCodigo.setText("");
					txtNombre.setText("");
					txtEspecializacion.setText("");
				}
			}
		});
		
		JLabel lblEspecialidad = new JLabel("Especialidad");
		panel.add(lblEspecialidad);
		
		txtEspecializacion = new JTextField();
		panel.add(txtEspecializacion);
		txtEspecializacion.setColumns(10);
		txtEspecializacion.addKeyListener(new KeyAdapter() {			
		    public void keyTyped(KeyEvent e) { 
		    	
		        if (txtEspecializacion.getText().length() >= especializacionMaximaLongitud ) //Limitar caracteres
		            e.consume(); 
		    }  
		});
		panel.add(btnIngresar);
		
		modeltable = new DefaultTableModel();
		table = new JTable(modeltable);
		JScrollPane scrolltable = new JScrollPane(table);
		getContentPane().add(scrolltable, BorderLayout.CENTER);
		initialize();
		
		//Tabla
		modeltable.addColumn("C�digo");
		modeltable.addColumn("Nombre");
		modeltable.addColumn("Especializacion");
		cargarMedicos();		
	}

	private void initialize() {
		setBounds(100, 100, 640, 406);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	private void cargarMedicos(){
		ArrayList<Medico> medicos = MedicoControlador.obtenerMedicos();
		for (Medico medico : medicos) {
			agregarMedico(medico.getCodigo(), medico.getNombre(), medico.getEspecializacion());
		}
	}
	private void agregarMedico(String codigo, String nombre, String especialidad){
		modeltable.addRow(new Object[] {codigo, nombre, especialidad});
	}
}
