package analisisdesoftware.tp.controlpacientes.controladores;

import java.util.ArrayList;

import analisisdesoftware.tp.controlpacientes.modelos.clasesauxiliares.Respuesta;
import analisisdesoftware.tp.controlpacientes.modelos.usuario.UsuarioModelo;

/** Gestiona los mensajes que realizan los usuarios del sistema*/
public class UsuarioControlador {
	
	/** Valida si el usuario esta registrado y tiene permitido el acceso al sistema
	 * @param usuario Nombre del usuario
	 * @param password Password del usuario
	 * @return Indica si el usuario es o no valido */
	public static boolean loguear(String usuario, String password){
		return UsuarioModelo.loguear(usuario, password);
	}
	
	/** Obtener longitud maxima que puede tener un diagnostico
	 * @return Longitud maxima que puede tener un diagnostico */
	public static int getLongitudDiagnosticoMaxima(){
		return UsuarioModelo.getLongitudDiagnosticoMaxima();
	}
	
	/** Agrega una nueva situacion de paciente 
	 * @param codigo_paciente Codigo del paciente atendido
	 * @param codigo_medico Codigo del medico que atendio
	 * @param diagnostico Diagnostico del medico
	 * @return Indica si pudo o no realizar la insercion y asocia un mensaje donde se detalla el motivo del resultado */
	public static Respuesta insertarSituacionPaciente(String codigo_paciente, String codigo_medico, String diagnostico){
		return UsuarioModelo.insertarSituacionPaciente(codigo_paciente, codigo_medico, diagnostico);
	}
	
	/** Obtiene la lista de pacientes atendidos por un medico
	 * @param codigo_medico Codigo del medico
	 * @return Lista con el nombre de los pacientes atendidos por el medico */
	public static ArrayList<String> obtenerPacientesAtendidos(String codigo_medico){
		return UsuarioModelo.obtenerPacientesAtendidos(codigo_medico);
	}
	/** Obtiene la lista de enfermedades que atendio un medico
	 * @param codigo_medico Codigo del medico
	 * @return Lista de diagnosticos que el medico determino*/
	public static ArrayList<String> obtenerEnfermedadesAtendidas(String codigo_medico){
		return UsuarioModelo.obtenerEnfermedadesAtendidas(codigo_medico);
	}
}
