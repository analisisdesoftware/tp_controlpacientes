package analisisdesoftware.tp.controlpacientes.controladores;

import java.util.ArrayList;

import analisisdesoftware.tp.controlpacientes.modelos.clasesauxiliares.Respuesta;
import analisisdesoftware.tp.controlpacientes.modelos.paciente.Paciente;
import analisisdesoftware.tp.controlpacientes.modelos.paciente.PacienteModelo;

/** Gestiona los mensajes relacionados a los pacientes */
public class PacienteControlador {
	/** Obtiene la longitud que debe tener el codigo de paciente
	 * @return Longitud del codigo de medico */
	public static int getLongitudCodigo(){
		return PacienteModelo.getLongitudCodigo();
	}
	/** Obtiene la longitud maxima que puede tener el nombre de paciente
	 * @return Longitud maxima del nombre de paciente*/
	public static int getLongitudNombreMaxima(){
		return PacienteModelo.getLongitudNombreMaxima();
	}
	
	/** Agrega un nuevo paciente
	 * @param codigo Codigo del paciente
	 * @param nombre Nombre del paciente
	 * @return Indica si pudo o no realizar la insercion y asocia un mensaje donde se detalla el motivo del resultado */
	public static Respuesta insertarPaciente(String codigo, String nombre){
		return PacienteModelo.insertarPaciente(codigo, nombre);
	}
	/** Obtiene el listado completo de pacientes
	 * @return Lista de pacientes */
	public static ArrayList<Paciente> obtenerPacientes(){
		return PacienteModelo.obtenerPacientes();
	}
}
