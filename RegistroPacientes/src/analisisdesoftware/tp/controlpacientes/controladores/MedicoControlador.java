package analisisdesoftware.tp.controlpacientes.controladores;

import java.util.ArrayList;

import analisisdesoftware.tp.controlpacientes.modelos.clasesauxiliares.Respuesta;
import analisisdesoftware.tp.controlpacientes.modelos.medico.Medico;
import analisisdesoftware.tp.controlpacientes.modelos.medico.MedicoModelo;

/** Gestiona los mensajes relacionados a los medicos */
public class MedicoControlador {
	/** Obtiene la longitud que debe tener el codigo de medico
	 * @return Longitud del codigo de medico */
	public static int getLongitudCodigo(){
		return MedicoModelo.getLongitudCodigo();
	}
	/** Obtiene la longitud maxima que puede tener el nombre de medico
	 * @return Longitud maxima del nombre de medico*/
	public static int getLongitudNombreMaxima(){
		return MedicoModelo.getLongitudNombreMaxima();
	}
	/** Obtiene la longitud maxima que puede tener la especializacion de medico
	 * @return Longitud maxima de la especializacion de medico*/
	public static int getLongitudEspecializacionMaxima(){
		return MedicoModelo.getLongitudEspecializacionMaxima();
	}
	
	/** Agrega un nuevo medico
	 * @param codigo Codigo del medico
	 * @param nombre Nombre del medico
	 * @param especializacion Especializacion del medico
	 * @return Indica si pudo o no realizar la insercion y asocia un mensaje donde se detalla el motivo del resultado */
	public static Respuesta insertarMedico(String codigo, String nombre, String especializacion){
		return MedicoModelo.insertarMedico(codigo, nombre, especializacion);
	}
	
	/** Obtiene el listado completo de medicos
	 * @return Lista de medicos */
	public static ArrayList<Medico> obtenerMedicos(){
		return MedicoModelo.obtenerMedicos();
	}
}
