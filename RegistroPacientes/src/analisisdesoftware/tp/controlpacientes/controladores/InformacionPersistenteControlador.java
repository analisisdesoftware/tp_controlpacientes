package analisisdesoftware.tp.controlpacientes.controladores;

import analisisdesoftware.tp.controlpacientes.modelos.informacionpersistente.InformacionPersistente;

/** Gestiona los mensajes con el almacen de datos */
public class InformacionPersistenteControlador {
	/** Inicializa el acceso a los datos. En caso de no existir el almacen de datos, lo crea para el primer uso
	 * @return Si pudo o no inicializar el acceso a la informacion */
	public static boolean InicializarAccesoInformacion(){
		return InformacionPersistente.inicializarBaseDatos();
	}
}
