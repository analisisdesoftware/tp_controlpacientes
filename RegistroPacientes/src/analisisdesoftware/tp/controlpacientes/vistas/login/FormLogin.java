package analisisdesoftware.tp.controlpacientes.vistas.login;

import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import javax.swing.SwingConstants;

import analisisdesoftware.tp.controlpacientes.controladores.InformacionPersistenteControlador;
import analisisdesoftware.tp.controlpacientes.controladores.UsuarioControlador;
import analisisdesoftware.tp.controlpacientes.vistas.principal.FormPrincipal;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;

/** Vista inicial del software, permite autenticar al usuario para ingresar al sistema */
public class FormLogin extends JFrame {

	/** Serializacion */
	private static final long serialVersionUID = -3883320047538252220L;
	
	/** Instancia singleton */
	private static FormLogin instance;
	/** Obtiene la instancia singleton 
	 * @return Instancia singleton */
	public static FormLogin getInstance(){ 
		return instance;
	}
	
	/** Nombre del usuario */
	private JTextField txtUsuario;
	/** Password del usuario */
	private JPasswordField txtPassword;
	/** Boton que inicia la accion de login */
	private JButton btnIngresar;

	/** Inicia el software */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FormLogin window = new FormLogin();
					window.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/** Vista inicial del software, permite autenticar al usuario para ingresar al sistema */
	private FormLogin() {
		boolean accesocreado;
		initialize();
		//Inicializar acceso a datos:
		accesocreado = InformacionPersistenteControlador.InicializarAccesoInformacion();
		//Singleton
		instance = this;
		//Analizar si se cargo el acceso a datos
		if(!accesocreado){																//Si no se pudo inicializar el almacen de datos
			cambiarDisponibilidadComponentes(false);									//Deshabilitar componentes graficos
			JOptionPane.showMessageDialog(null, "Error al iniciar almacen de datos");	//Informar
		}
	}

	/** Inicializa los componentes graficos*/
	private void initialize() {
		setTitle("Centro M\u00E9dico Los Laureles - Control de Pacientes");
		setResizable(false);
		setBounds(100, 100, 399, 227);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		setIconImage(Toolkit.getDefaultToolkit().getImage(FormLogin.class.getResource("/res/icon_hospital.png")));
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		
		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setBounds(26, 29, 338, 14);
		getContentPane().add(lblUsuario);
		lblUsuario.setVerticalAlignment(SwingConstants.BOTTOM);
		
		JLabel lblPassword = new JLabel("Contrase\u00F1a");
		lblPassword.setBounds(26, 87, 338, 14);
		getContentPane().add(lblPassword);
		lblPassword.setVerticalAlignment(SwingConstants.BOTTOM);
		
		txtUsuario = new JTextField();
		txtUsuario.setBounds(26, 45, 338, 20);
		getContentPane().add(txtUsuario);
		txtUsuario.setHorizontalAlignment(SwingConstants.LEFT);
		txtUsuario.setColumns(10);
		
		txtPassword = new JPasswordField();
		txtPassword.setBounds(26, 102, 339, 20);
		getContentPane().add(txtPassword);
		txtPassword.setHorizontalAlignment(SwingConstants.LEFT);
		
		btnIngresar = new JButton("Ingresar");
		btnIngresar.setBounds(146, 153, 109, 20);
		getContentPane().add(btnIngresar);
		btnIngresar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				loguear();
			}
		});
	}

	/** Cambia la habilitacion de la vista 
	 * @param habilitar Indica si la vista estara habilitada o no*/
	private void cambiarDisponibilidadComponentes(boolean habilitar){
		btnIngresar.setEnabled(habilitar);
		txtUsuario.setEnabled(habilitar);
		txtPassword.setEnabled(habilitar);
	}
	
	/** Intenta loguear al usuario con los datos ingresados en la vista */
	private void loguear(){
		String usuario = txtUsuario.getText().trim();
		String password = new String( txtPassword.getPassword());
		boolean logueado;
		//Validar
		if(usuario.length() == 0){
			txtUsuario.requestFocus();
			JOptionPane.showMessageDialog(null, "Ingrese su nombre de usuario");	// Informar
			return;
		}
		if(password.length() == 0){
			txtPassword.requestFocus();
			JOptionPane.showMessageDialog(null, "Ingrese su nombre de contrase�a");	// Informar
			return;
		}
		//Intentar loguear
		cambiarDisponibilidadComponentes(false);					//Deshabilitar los componentes de la vista
	    logueado = UsuarioControlador.loguear(usuario, password);	//Intentar loguear en el sistema
		if(logueado){												//Si se logueo correctamente
			FormPrincipal frm = new FormPrincipal();				// Crear vista principal
			frm.setVisible(true);									// Mostrar FormPrincipal
			FormLogin.this.setVisible(false);						// Ocultar Login
			txtPassword.setText("");								// Limpiar password
		}else{														//Si no se logueo correctamente
			JOptionPane.showMessageDialog(null, "Usuario inv�lido");// Informar
		}
		cambiarDisponibilidadComponentes(true);						//Habilitar componetes de la vista
	}
}
