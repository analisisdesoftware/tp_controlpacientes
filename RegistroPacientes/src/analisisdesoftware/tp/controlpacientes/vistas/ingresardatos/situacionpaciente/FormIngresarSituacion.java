package analisisdesoftware.tp.controlpacientes.vistas.ingresardatos.situacionpaciente;

import javax.swing.JDialog;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JTextField;

import analisisdesoftware.tp.controlpacientes.controladores.MedicoControlador;
import analisisdesoftware.tp.controlpacientes.controladores.PacienteControlador;
import analisisdesoftware.tp.controlpacientes.controladores.UsuarioControlador;
import analisisdesoftware.tp.controlpacientes.modelos.clasesauxiliares.Respuesta;
import analisisdesoftware.tp.controlpacientes.modelos.medico.Medico;
import analisisdesoftware.tp.controlpacientes.modelos.paciente.Paciente;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;

/** Vista destinada al ingreso de nuevas situaciones de pacientes */
public class FormIngresarSituacion extends JDialog{
	
	/** Serializacion */
	private static final long serialVersionUID = 1956306470327524280L;
	
	/** Cantidad de caracteres maximos para el diagnostico */
	private static int diagnosticoMaximaLongitud;
	
	/** Combobox de pacientes disponibles en el sistema */
	private JComboBox<Paciente> cbxPaciente;
	/** Combobox de medicos disponibles en el sistema */
	private JComboBox<Medico> cbxMedico;
	/** Diagnostico realizado por el medico */
	private JTextField txtDiagnostico;
	/** Boton que inicia la insercion de la situacion del paciente */
	private JButton btnIngresar;
	
	/** Vista destinada al ingreso de nuevas situaciones de pacientes */
	public FormIngresarSituacion() {
		ArrayList<Paciente> pacientes;
		ArrayList<Medico> medicos;
		initialize();
		//Cargar datos
		diagnosticoMaximaLongitud = UsuarioControlador.getLongitudDiagnosticoMaxima();	//Cantidad de caracteres maximos para el diagnostico
		pacientes = PacienteControlador.obtenerPacientes();								//Obtener pacientes disponibles en el sistema
		medicos = MedicoControlador.obtenerMedicos();									//Obtener medicos disponibles en el sistema
		cargarPacientes(pacientes);														//Cargar pacientes en la vista
		cargarMedicos(medicos);															//Cargar medicos en la vista
		if(pacientes.size() == 0 || medicos.size() == 0){								//Si no hay pacientes o medicos en el sistema
			txtDiagnostico.setEnabled(false);											// Deshabilitar ingreso de diagnosticos
			btnIngresar.setEnabled(false);												// Deshabilitar boton que inicia la insercion
		}
	}

	/** Inicializa los componentes graficos de la vista */
	private void initialize() {
		setBounds(100, 100, 476, 157);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setResizable(false);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setTitle("Centro M\u00E9dico Los Laureles - Situaci\u00F3n Paciente");
		setIconImage(Toolkit.getDefaultToolkit().getImage(FormIngresarSituacion.class.getResource("/res/icon_hospital.png")));
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		
		btnIngresar = new JButton("Ingresar");
		btnIngresar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ingresarSituacionPaciente();
			}
		});
		getContentPane().add(btnIngresar, BorderLayout.SOUTH);
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		JLabel lblPaciente = new JLabel("Paciente");
		lblPaciente.setBounds(6, 9, 55, 14);
		panel.add(lblPaciente);
		
		cbxPaciente = new JComboBox<Paciente>();
		cbxPaciente.setBounds(81, 6, 379, 20);
		panel.add(cbxPaciente);
		
		JLabel lblMedico = new JLabel("M\u00E9dico");
		lblMedico.setBounds(6, 35, 55, 14);
		panel.add(lblMedico);
		
		cbxMedico = new JComboBox<Medico>();
		cbxMedico.setBounds(81, 32, 379, 20);
		panel.add(cbxMedico);
		
		JLabel lblDiagnstico = new JLabel("Diagn\u00F3stico");
		lblDiagnstico.setBounds(6, 61, 68, 14);
		panel.add(lblDiagnstico);
		
		txtDiagnostico = new JTextField();
		txtDiagnostico.setBounds(81, 58, 379, 20);
		panel.add(txtDiagnostico);
		txtDiagnostico.setColumns(10);
		txtDiagnostico.addKeyListener(new KeyAdapter() {			
		    public void keyTyped(KeyEvent e) { 
		        if (txtDiagnostico.getText().length() >= diagnosticoMaximaLongitud ) //Limitar caracteres
		            e.consume(); 
		    }  
		});
	}
	
	/** Carga la lista de pacientes en la vista
	 * @param pacientes Lista de pacientes */
	private void cargarPacientes(ArrayList<Paciente> pacientes){
		for (Paciente paciente : pacientes) {
			cbxPaciente.addItem(paciente);
		}
	}
	/** Carga la lista de medicos en la vista
	 * @param medicos Lista de medicos */
	private void cargarMedicos(ArrayList<Medico> medicos){
		for (Medico medico : medicos) {
			cbxMedico.addItem(medico);
		}
	}
	/** Inserta la situacion de paciente ingresada en la vista en el sistema */
	private void ingresarSituacionPaciente(){
		Paciente paciente = (Paciente) cbxPaciente.getSelectedItem();
		Medico medico = (Medico) cbxMedico.getSelectedItem();
		String diagnostico = txtDiagnostico.getText().trim();
		//Validaciones
		if(diagnostico.length() == 0){												//Si no se ingreso un diagnostico
			JOptionPane.showMessageDialog(null, "Debe ingresar el diagnůstico");
			txtDiagnostico.requestFocus();
			return;
		}
		//Insertar
		Respuesta respuesta = UsuarioControlador.insertarSituacionPaciente(paciente.getCodigo(), medico.getCodigo(), diagnostico);	//Inicia la insercion en el sistema
		JOptionPane.showMessageDialog(null, respuesta.getMensaje());																//Muestra el resultado
		if(respuesta.fueSatisfactoria()){																							//Si se ingreso correctamente
			txtDiagnostico.setText("");																								// Limpiar diagnostico
			cbxPaciente.requestFocus();																								// Poner en foco el combo de pacientes
		}
	}
}
