package analisisdesoftware.tp.controlpacientes.vistas.ingresardatos.datospaciente;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JTable;
import java.awt.FlowLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import analisisdesoftware.tp.controlpacientes.controladores.PacienteControlador;
import analisisdesoftware.tp.controlpacientes.modelos.clasesauxiliares.Respuesta;
import analisisdesoftware.tp.controlpacientes.modelos.paciente.Paciente;

import javax.swing.JButton;
import javax.swing.JDialog;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;

/** Vista destinada al ingreso de nuevos pacientes */
public class FormIngresoPaciente extends JDialog {
	/** Serializacion */
	private static final long serialVersionUID = -9186227452239316406L;
	
	/** Modelo de la tabla. Define las columnas y filas que tendra la tabla */
	private DefaultTableModel modeltable;
	/** Codigo del paciente */
	private JTextField txtCodigo;
	/** Nombre del paciente */
	private JTextField txtNombre;
	
	/** Cantidad de caracteres para el codigo */
	private static int codigoLongitud;
	/** Cantidad de caracteres maximos para el nombre */
	private static int nombreMaximaLongitud;
	
	/** Vista destinada al ingreso de nuevos pacientes */
	public FormIngresoPaciente() {
		initialize();
		//Cargar longitudes
		codigoLongitud = PacienteControlador.getLongitudCodigo();				//Cantidad de caracteres para el codigo
		nombreMaximaLongitud = PacienteControlador.getLongitudNombreMaxima();	//Cantidad de caracteres maximos para el nombre
		//Tabla
		cargarPacientes();		
	}

	/** Inicializa componentes graficos */
	private void initialize() {
		setBounds(100, 100, 450, 300);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setTitle("Centro M\u00E9dico Los Laureles - Datos de Pacientes");
		setIconImage(Toolkit.getDefaultToolkit().getImage(FormIngresoPaciente.class.getResource("/res/icon_hospital.png")));
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.NORTH);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel lblCodigo = new JLabel("C\u00F3digo");
		panel.add(lblCodigo);
		
		txtCodigo = new JTextField();
		panel.add(txtCodigo);
		txtCodigo.setColumns(10);
		txtCodigo.addKeyListener(new KeyAdapter() {
		    public void keyTyped(KeyEvent e) { 
		        if (txtCodigo.getText().length() == codigoLongitud ) //Limitar caracteres
		            e.consume(); 
		    }  
		});
		
		JLabel lblNombre = new JLabel("Nombre");
		panel.add(lblNombre);
		
		txtNombre = new JTextField();
		txtNombre.setText("");
		panel.add(txtNombre);
		txtNombre.setColumns(10);
		txtNombre.addKeyListener(new KeyAdapter() {			
		    public void keyTyped(KeyEvent e) { 
		    	
		        if (txtNombre.getText().length() >= nombreMaximaLongitud ) //Limitar caracteres
		            e.consume(); 
		    }  
		});
		
		JButton btnIngresar = new JButton("Ingresar");
		btnIngresar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				insertarPaciente();
			}
		});
		panel.add(btnIngresar);
		
		modeltable = new DefaultTableModel();
		JTable table = new JTable(modeltable);
		JScrollPane scrolltable = new JScrollPane(table);
		getContentPane().add(scrolltable, BorderLayout.CENTER);
		modeltable.addColumn("C�digo");
		modeltable.addColumn("Nombre");
	}
	
	/** Carga los pacientes actuales en el sistema en la vista */
	private void cargarPacientes(){
		ArrayList<Paciente> pacientes = PacienteControlador.obtenerPacientes();	//Obtiene todos los pacientes actuales en el sistema
		for (Paciente paciente : pacientes) {
			agregarPaciente(paciente.getCodigo(), paciente.getNombre());
		}
	}
	/** Inserta el paciente ingresado en la vista en el sistema */
	private void insertarPaciente(){
		String codigo = txtCodigo.getText().trim();
		String nombre = txtNombre.getText().trim();
		//Validar que haya caracteres
		if(codigo.length() != codigoLongitud){
			JOptionPane.showMessageDialog(null, "Ingrese el c�digo de paciente de " + codigoLongitud + " caracteres");
			return;
		}
		if(nombre.length() == 0 || nombre.length() > nombreMaximaLongitud){
			JOptionPane.showMessageDialog(null, "El nombre del paciente debe tener como mucho " + nombreMaximaLongitud + " caracteres");
			return;
		}
		//Insertar
		Respuesta respuesta = PacienteControlador.insertarPaciente(codigo, nombre);
		JOptionPane.showMessageDialog(null, respuesta.getMensaje());
		if(respuesta.fueSatisfactoria()){
			agregarPaciente(codigo, nombre);
			txtCodigo.setText("");
			txtNombre.setText("");
		}
	}	
	/** Agrega un paciente en la vista
	 * @param codigo Codigo del paciente
	 * @param nombre Nombre del paciente*/
	private void agregarPaciente(String codigo, String nombre){
		modeltable.addRow(new Object[] {codigo, nombre});
	}
}
