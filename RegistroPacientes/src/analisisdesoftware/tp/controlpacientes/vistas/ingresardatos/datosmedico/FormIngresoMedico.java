package analisisdesoftware.tp.controlpacientes.vistas.ingresardatos.datosmedico;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import analisisdesoftware.tp.controlpacientes.controladores.MedicoControlador;
import analisisdesoftware.tp.controlpacientes.modelos.clasesauxiliares.Respuesta;
import analisisdesoftware.tp.controlpacientes.modelos.medico.Medico;

import java.awt.Toolkit;

/** Vista destinada al ingreso de nuevos medicos */
public class FormIngresoMedico extends JDialog{

	/** Serializacion */
	private static final long serialVersionUID = -5920247280997044345L;
	
	/** Modelo de la tabla. Define las columnas y filas que tendra la tabla */
	private DefaultTableModel modeltable;
	/** Codigo del medico */
	private JTextField txtCodigo;
	/** Nombre del medico */
	private JTextField txtNombre;
	/** Especializacion del medico */
	private JTextField txtEspecializacion;
	
	/** Cantidad de caracteres para el codigo */
	private static int codigoLongitud;
	/** Cantidad de caracteres maximos para el nombre */
	private static int nombreMaximaLongitud;
	/** Cantidad de caracteres maximos para la especializacion */
	private static int especializacionMaximaLongitud;
	
	/** Vista destinada al ingreso de nuevos medicos */
	public FormIngresoMedico() {
		initialize();
		//Cargar longitudes
		codigoLongitud = MedicoControlador.getLongitudCodigo();									//Cantidad de caracteres para el codigo
		nombreMaximaLongitud = MedicoControlador.getLongitudNombreMaxima();						//Cantidad de caracteres maximos para el nombre
		especializacionMaximaLongitud = MedicoControlador.getLongitudEspecializacionMaxima();	//Cantidad de caracteres maximos para la especializacion
		//Tabla
		cargarMedicos();		
	}
	/** Inicializa componentes graficos */
	private void initialize() {
		setBounds(100, 100, 640, 406);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setTitle("Centro M\u00E9dico Los Laureles - Datos de M�dicos");
		setIconImage(Toolkit.getDefaultToolkit().getImage(FormIngresoMedico.class.getResource("/res/icon_hospital.png")));	
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.NORTH);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel lblCodigo = new JLabel("C\u00F3digo");
		panel.add(lblCodigo);
		
		txtCodigo = new JTextField();
		panel.add(txtCodigo);
		txtCodigo.setColumns(10);
		txtCodigo.addKeyListener(new KeyAdapter() {
		    public void keyTyped(KeyEvent e) { 
		        if (txtCodigo.getText().length() == codigoLongitud ) //Limitar caracteres
		            e.consume(); 
		    }  
		});
		
		JLabel lblNombre = new JLabel("Nombre");
		panel.add(lblNombre);
		
		txtNombre = new JTextField();
		txtNombre.setText("");
		panel.add(txtNombre);
		txtNombre.setColumns(10);
		txtNombre.addKeyListener(new KeyAdapter() {			
		    public void keyTyped(KeyEvent e) { 
		    	
		        if (txtNombre.getText().length() >= nombreMaximaLongitud ) //Limitar caracteres
		            e.consume(); 
		    }  
		});
		
		JButton btnIngresar = new JButton("Ingresar");
		btnIngresar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				insertarMedico();
			}
		});
		
		JLabel lblEspecialidad = new JLabel("Especialidad");
		panel.add(lblEspecialidad);
		
		txtEspecializacion = new JTextField();
		panel.add(txtEspecializacion);
		txtEspecializacion.setColumns(10);
		txtEspecializacion.addKeyListener(new KeyAdapter() {			
		    public void keyTyped(KeyEvent e) { 
		    	
		        if (txtEspecializacion.getText().length() >= especializacionMaximaLongitud ) //Limitar caracteres
		            e.consume(); 
		    }  
		});
		panel.add(btnIngresar);
		
		modeltable = new DefaultTableModel();
		JTable table = new JTable(modeltable);
		JScrollPane scrolltable = new JScrollPane(table);
		getContentPane().add(scrolltable, BorderLayout.CENTER);
		modeltable.addColumn("C�digo");
		modeltable.addColumn("Nombre");
		modeltable.addColumn("Especializacion");
	}
	/** Carga los medicos actuales en el sistema en la vista */
	private void cargarMedicos(){
		ArrayList<Medico> medicos = MedicoControlador.obtenerMedicos();
		for (Medico medico : medicos) {
			agregarMedico(medico.getCodigo(), medico.getNombre(), medico.getEspecializacion());
		}
	}
	/** Inserta el medico ingresado en la vista en el sistema */
	private void insertarMedico(){
		String codigo = txtCodigo.getText().trim();
		String nombre = txtNombre.getText().trim();
		String especialidad = txtEspecializacion.getText().trim();
		//Validar que haya caracteres
		if(codigo.length() != codigoLongitud){
			JOptionPane.showMessageDialog(null, "Ingrese el c�digo de medico de " + codigoLongitud + " caracteres");
			return;
		}
		if(nombre.length() == 0 || nombre.length() > nombreMaximaLongitud){
			JOptionPane.showMessageDialog(null, "El nombre del medico debe tener como mucho " + nombreMaximaLongitud + " caracteres");
			return;
		}
		if(especialidad.length() == 0 || especialidad.length() > especializacionMaximaLongitud){
			JOptionPane.showMessageDialog(null, "La especializacion del medico debe tener como mucho " + especializacionMaximaLongitud + " caracteres");
			return;
		}
		//Insertar
		Respuesta respuesta = MedicoControlador.insertarMedico(codigo, nombre, especialidad);
		JOptionPane.showMessageDialog(null, respuesta.getMensaje());
		if(respuesta.fueSatisfactoria()){
			agregarMedico(codigo, nombre, especialidad);
			txtCodigo.setText("");
			txtNombre.setText("");
			txtEspecializacion.setText("");
		}
	}
	/** Agrega un medico en la vista
	 * @param codigo Codigo del medico
	 * @param nombre Nombre del medico
	 * @param especializacion Especializacion del medico*/
	private void agregarMedico(String codigo, String nombre, String especializacion){
		modeltable.addRow(new Object[] {codigo, nombre, especializacion});
	}
}
