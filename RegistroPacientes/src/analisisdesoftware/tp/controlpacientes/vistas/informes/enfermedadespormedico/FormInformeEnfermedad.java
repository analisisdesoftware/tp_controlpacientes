package analisisdesoftware.tp.controlpacientes.vistas.informes.enfermedadespormedico;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import analisisdesoftware.tp.controlpacientes.controladores.MedicoControlador;
import analisisdesoftware.tp.controlpacientes.controladores.UsuarioControlador;
import analisisdesoftware.tp.controlpacientes.modelos.medico.Medico;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.ArrayList;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;

/** Vista para consultar informes de enfermedades atendidas por medicos */
public class FormInformeEnfermedad extends JDialog{
	/** Serializacion */
	private static final long serialVersionUID = -1577351613722069829L;
	
	/** Modelo de la tabla. Define las columnas y filas que tendra la tabla */
	private DefaultTableModel modeloTabla;
	/** Combobox de medicos */
	private JComboBox<Medico> cbxMedico;
	/** Boton que realiza inicia la consulta */
	private JButton btnConsultar;
	
	/** Vista para consultar informes de enfermedades atendidas por medicos */
	public FormInformeEnfermedad() {
		ArrayList<Medico> medicos;
		initialize();
		//Cargar medicos
		medicos = MedicoControlador.obtenerMedicos();	//Obtiene el listado de medicos
		if(medicos.size() > 0){							//Si hay medicos cargados en el sistema
			cargarMedicos(medicos);						// Cargar medicos
		}else{											//Si no hay medicos cargados en el sistema
			btnConsultar.setEnabled(false);				// Deshabilitar el boton de consultar
		}
	}

	/** Inicializa componentes graficos*/
	private void initialize() {
		setTitle("Centro M\u00E9dico Los Laureles - Enfermedades que atiende cada m\u00E9dico");
		setModalityType(ModalityType.APPLICATION_MODAL);
		setBounds(100, 100, 640, 480);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setIconImage(Toolkit.getDefaultToolkit().getImage(FormInformeEnfermedad.class.getResource("/res/icon_hospital.png")));
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.NORTH);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{65, 28, 0, 0, 0, 0, 0, 0, 70, 73, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel.rowHeights = new int[]{28, 0};
		gbl_panel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel lblMdico = new JLabel("M\u00E9dico");
		GridBagConstraints gbc_lblMdico = new GridBagConstraints();
		gbc_lblMdico.insets = new Insets(0, 0, 0, 5);
		gbc_lblMdico.gridx = 0;
		gbc_lblMdico.gridy = 0;
		panel.add(lblMdico, gbc_lblMdico);
		
		cbxMedico = new JComboBox<Medico>();
		GridBagConstraints gbc_cbxMedico = new GridBagConstraints();
		gbc_cbxMedico.fill = GridBagConstraints.HORIZONTAL;
		gbc_cbxMedico.gridwidth = 13;
		gbc_cbxMedico.insets = new Insets(0, 0, 0, 5);
		gbc_cbxMedico.gridx = 1;
		gbc_cbxMedico.gridy = 0;
		panel.add(cbxMedico, gbc_cbxMedico);
		
		btnConsultar = new JButton("Consultar");
		btnConsultar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				consultarEnfermedadesTratadas();
			}
		});
		GridBagConstraints gbc_btnConsultar = new GridBagConstraints();
		gbc_btnConsultar.anchor = GridBagConstraints.EAST;
		gbc_btnConsultar.gridx = 15;
		gbc_btnConsultar.gridy = 0;
		panel.add(btnConsultar, gbc_btnConsultar);
		
		modeloTabla = new DefaultTableModel();
		JTable table = new JTable(modeloTabla);
		JScrollPane scrolltable = new JScrollPane(table);
		getContentPane().add(scrolltable, BorderLayout.CENTER);
		modeloTabla.addColumn("Enfermedades que trat� el m�dico");
	}
	
	/** Carga el listado de medicos en la vista 
	 * @param medicos Lista de medicos a cargar*/
	private void cargarMedicos(ArrayList<Medico> medicos){
		for (Medico medico : medicos) {
			cbxMedico.addItem(medico);
		}
	}
	
	/** Consulta las enfermedades tratadas por el medico indicado en la vista */
	private void consultarEnfermedadesTratadas(){
		Medico medico = (Medico)cbxMedico.getSelectedItem();													//Obtiene el medico seleccionado
		ArrayList<String> enfermedades = UsuarioControlador.obtenerEnfermedadesAtendidas(medico.getCodigo());	//Consulta las enfermedades
		modeloTabla.setRowCount(0);																				//Limpia las filas de la tabla
		for (String enfermedad : enfermedades) {
			modeloTabla.addRow(new Object[]{enfermedad});
		}
	}
}
