package analisisdesoftware.tp.controlpacientes.modelos.medico;

import java.util.ArrayList;

import analisisdesoftware.tp.controlpacientes.modelos.clasesauxiliares.Respuesta;
import analisisdesoftware.tp.controlpacientes.modelos.informacionpersistente.InformacionPersistente;

/** Logica y acceso a datos de los medicos */
public class MedicoModelo {
	/** Obtiene la longitud que debe tener el codigo de medico
	 * @return Longitud del codigo de medico */
	public static int getLongitudCodigo(){
		return Medico.CODIGO_LONGITUD;
	}
	/** Obtiene la longitud maxima que puede tener el nombre de medico
	 * @return Longitud maxima del nombre de medico*/
	public static int getLongitudNombreMaxima(){
		return Medico.NOMBRE_LONGITUD_MAXIMA;
	}
	/** Obtiene la longitud maxima que puede tener la especializacion de medico
	 * @return Longitud maxima de la especializacion de medico*/
	public static int getLongitudEspecializacionMaxima(){
		return Medico.ESPECIALIZACION_LONGITUD_MAXIMA;
	}
	
	/** Agrega un nuevo medico al almacen de datos
	 * @param codigo Codigo del medico
	 * @param nombre Nombre del medico
	 * @param especializacion Especializacion del medico
	 * @return Indica si pudo o no realizar la insercion y asocia un mensaje donde se detalla la accion */
	public static Respuesta insertarMedico(String codigo, String nombre, String especializacion){
		//Validar entrada
		if(!Medico.validarLongitudCodigo(codigo))
			return new Respuesta(false, "El c�digo debe tener " + Medico.CODIGO_LONGITUD + " caracteres");
		if(!Medico.validarLongitudNombre(nombre))
			return new Respuesta(false, "El nombre puede tener como m�ximo " + Medico.NOMBRE_LONGITUD_MAXIMA + " caracteres");
		if(!Medico.validarLongitudEspecializacion(especializacion))
			return new Respuesta(false, "La especializaci�n puede tener como m�ximo " + Medico.ESPECIALIZACION_LONGITUD_MAXIMA + " caracteres");
		//Validar codigo existente
		if(InformacionPersistente.existeMedico(codigo))
			return new Respuesta(false, "Ya existe un m�dico con ese c�digo");
		//Insertar Paciente
		if(InformacionPersistente.insertarMedico(codigo, nombre, especializacion)){
			return new Respuesta(true, "M�dico " + nombre + " ingresado correctamente");									//Ingresado correctamente
		}else{
			return new Respuesta(false, "No pudo ingresarse el m�dico " + nombre + ". Cont�ctese con el administrador");	//No pudo ingresarse por excepcion
		}
	}
	/** Obtiene el listado completo de medicos
	 * @return Lista de medicos */
	public static ArrayList<Medico> obtenerMedicos(){
		return InformacionPersistente.obtenerMedicos();
	}
}
