package analisisdesoftware.tp.controlpacientes.modelos.medico;

/** Representa medicos en el sistema */
public class Medico {
	/** Obtiene la longitud que debe tener el codigo de medico*/
	public final static int CODIGO_LONGITUD = 5;
	/** Obtiene la longitud maxima que puede tener el nombre de medico*/
	public final static int NOMBRE_LONGITUD_MAXIMA = 50;
	/** Obtiene la longitud maxima que puede tener la especializacion de medico*/
	public final static int ESPECIALIZACION_LONGITUD_MAXIMA = 50;
	
	/** Codigo del medico*/
	private String codigo;
	/** Nombre del medico*/
	private String nombre;
	/** Especializacion del medico*/
	private String especializacion;
	
	/** Codigo del medico
	 * @return Codigo del medico */
	public String getCodigo(){
		return this.codigo;
	}
	/** Nombre del medico
	 * @return Nombre del medico*/
	public String getNombre(){
		return this.nombre;
	}
	/** Especializacion del medico
	 * @return Especializacion del medico*/
	public String getEspecializacion(){
		return this.especializacion;
	}
	
	/** Representa medicos en el sistema
	 * @param codigo Codigo del medico
	 * @param nombre Nombre del medico
	 * @param especializacion Especializacion del medico*/
	public Medico(String codigo, String nombre, String especializacion){		
		this.codigo = codigo;
		this.nombre = nombre;
		this.especializacion = especializacion;
	}
	
	/** Representacion del medico en formato string
	 * @return Nombre y especializacion del medico */
	@Override
	public String toString(){
		return this.nombre + " (" + this.especializacion + ")";
	}
	/** Valida si es correcta la logitud que debe tener el codigo del medico
	 * @param codigo Codigo del medico
	 * @return True: El codigo tiene la longitud correcta. False: El codigo no tiene la longitud correcta */
	public static boolean validarLongitudCodigo(String codigo){
		return codigo.length() == CODIGO_LONGITUD;
	}
	/** Valida si es correcta la logitud que debe tener el nombre del medico
	 * @param nombre Nombre del medico
	 * @return True: El nombre tiene la longitud correcta. False: El nombre no tiene la longitud correcta */
	public static boolean validarLongitudNombre(String nombre){
		return nombre.length() <= NOMBRE_LONGITUD_MAXIMA;
	}
	/** Valida si es correcta la logitud que debe tener la especializacion del medico
	 * @param especializacion Especializacion del medico
	 * @return True: La especializacion tiene la longitud correcta. False: El especializacion no tiene la longitud correcta */
	public static boolean validarLongitudEspecializacion(String especializacion){
		return especializacion.length() <= ESPECIALIZACION_LONGITUD_MAXIMA;
	}
}
