package analisisdesoftware.tp.controlpacientes.modelos.paciente;

/** Representa pacientes en el sistema */
public class Paciente {
	/** Obtiene la longitud que debe tener el codigo de paciente*/
	public final static int CODIGO_LONGITUD = 5;
	/** Obtiene la longitud maxima que puede tener el nombre de paciente*/
	public final static int NOMBRE_LONGITUD_MAXIMA = 50;
	
	/** Codigo del paciente*/
	private String codigo;
	/** Nombre del paciente*/
	private String nombre;
	
	/** Codigo del paciente
	 * @return Codigo del paciente */
	public String getCodigo(){
		return this.codigo;
	}
	/** Nombre del paciente
	 * @return Nombre del paciente*/
	public String getNombre(){
		return this.nombre;
	}
	
	/** Representa pacientes en el sistema
	 * @param codigo Codigo del paciente
	 * @param nombre Nombre del paciente*/
	public Paciente(String codigo, String nombre){		
		this.codigo = codigo;
		this.nombre = nombre;
	}

	/** Representacion del paciente en formato string
	 * @return Nombre y especializacion del paciente */
	@Override
	public String toString(){
		return this.nombre;
	}
	
	/** Valida si es correcta la logitud que debe tener el codigo del paciente
	 * @param codigo Codigo del paciente
	 * @return True: El codigo tiene la longitud correcta. False: El codigo no tiene la longitud correcta */
	public static boolean validarLongitudCodigo(String codigo){
		return codigo.length() == CODIGO_LONGITUD;
	}
	/** Valida si es correcta la logitud que debe tener el nombre del paciente
	 * @param nombre Nombre del paciente
	 * @return True: El nombre tiene la longitud correcta. False: El nombre no tiene la longitud correcta */
	public static boolean validarLongitudNombre(String nombre){
		return nombre.length() <= NOMBRE_LONGITUD_MAXIMA;
	}
}
