package analisisdesoftware.tp.controlpacientes.modelos.paciente;

import java.util.ArrayList;

import analisisdesoftware.tp.controlpacientes.modelos.clasesauxiliares.Respuesta;
import analisisdesoftware.tp.controlpacientes.modelos.informacionpersistente.InformacionPersistente;

/** Logica y acceso a datos de los pacientes */
public class PacienteModelo {
	/** Obtiene la longitud que debe tener el codigo de paciente
	 * @return Longitud del codigo de medico */
	public static int getLongitudCodigo(){
		return Paciente.CODIGO_LONGITUD;
	}
	/** Obtiene la longitud maxima que puede tener el nombre de paciente
	 * @return Longitud maxima del nombre de paciente*/
	public static int getLongitudNombreMaxima(){
		return Paciente.NOMBRE_LONGITUD_MAXIMA;
	}
	
	/** Agrega un nuevo paciente
	 * @param codigo Codigo del paciente
	 * @param nombre Nombre del paciente
	 * @return Indica si pudo o no realizar la insercion y asocia un mensaje donde se detalla el motivo del resultado */
	public static Respuesta insertarPaciente(String codigo, String nombre){
		//Validar entrada
		if(!Paciente.validarLongitudCodigo(codigo))
			return new Respuesta(false, "El c�digo debe tener " + Paciente.CODIGO_LONGITUD + " caracteres");
		if(!Paciente.validarLongitudNombre(nombre))
			return new Respuesta(false, "El nombre puede tener como m�ximo " + Paciente.NOMBRE_LONGITUD_MAXIMA + " caracteres");
		//Validar codigo existente
		if(InformacionPersistente.existePaciente(codigo))
			return new Respuesta(false, "Ya existe un paciente con ese c�digo");
		//Insertar Paciente
		if(InformacionPersistente.insertarPaciente(codigo, nombre)){
			return new Respuesta(true, "Paciente " + nombre + " ingresado correctamente");									//Ingresado correctamente
		}else{
			return new Respuesta(false, "No pudo ingresarse el paciente " + nombre + ". Cont�ctese con el administrador");	//No pudo ingresarse por excepcion
		}
	}
	/** Obtiene el listado completo de pacientes
	 * @return Lista de pacientes */
	public static ArrayList<Paciente> obtenerPacientes(){
		return InformacionPersistente.obtenerPacientes();
	}
}
