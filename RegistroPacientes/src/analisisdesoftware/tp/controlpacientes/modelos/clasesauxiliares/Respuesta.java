package analisisdesoftware.tp.controlpacientes.modelos.clasesauxiliares;

/** Representa respuestas de satisfactoriedad describiendo el resultado. Se utiliza para comunicar las vistas, los controladores y los modelos */
public class Respuesta {
	/** Indica si la respuesta fue satisfactoria o no */
	private boolean satisfactoria;
	/** Describe el resultado */
	private String mensaje;
	
	/** Indica si la respuesta fue satisfactoria o no 
	 * @return True: satisfactoria. False: No satisfactoria*/
	public boolean fueSatisfactoria(){
		return this.satisfactoria;
	}
	/** Describe el resultado 
	 * @return descripcion del motivo de la satisfactoriedad*/
	public String getMensaje(){
		return this.mensaje;
	}
	
	/** Representa respuestas de satisfactoriedad describiendo el resultado. Se utiliza para comunicar las vistas, los controladores y los modelos
	 * @param satisfactorio Indica si la respuesta fue satisfactoria o no
	 * @param mensaje Describe el resultado*/
	public Respuesta(boolean satisfactorio, String mensaje){
		this.satisfactoria = satisfactorio;
		this.mensaje = mensaje;
	}
}
